import sys

from PySide6.QtGui import QAction
from PySide6.QtWidgets import QApplication, QWidget, QMainWindow, QHBoxLayout, QFileDialog

from controllers.controllers import LeftPanelController, RightPanelController, MenuBarController
from model.importmodel import ImportModel
from views.leftpanel import LeftPanel
from views.menubar import MenuBar
from views.rightpanel import RightPanel


class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Pycture Sorter')

        central_widget = QWidget()
        self.setCentralWidget(central_widget)

        self._mainLayout = QHBoxLayout(central_widget)

        left_panel = LeftPanel()
        self._mainLayout.addWidget(left_panel, 3)

        self.menuBar = MenuBar()
        self.setMenuBar(self.menuBar)
        # Ajout du panneau de gauche
        right_panel = RightPanel()
        self._mainLayout.addWidget(right_panel, 1)

        # controlleurs & model
        import_model = ImportModel()
        self.menuBarController = MenuBarController(import_model, self.menuBar)
        self.left_panel_controller = LeftPanelController(import_model, left_panel)
        import_model.addObserver(left_panel)
        self.right_panel_controller = RightPanelController(import_model, right_panel)
        import_model.addObserver(right_panel)

        import_model.set_directory_path("/home/julien/Téléchargements/chat&moi")


app = QApplication(sys.argv)
app.setStyle("Fusion")
screenSize = app.primaryScreen().size()  # Obtenir la taille de l'écran
window = Window()
# window.setMaximumSize(screenSize)
window.showMaximized()
sys.exit(app.exec())
