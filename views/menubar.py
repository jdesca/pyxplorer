from PySide6.QtGui import QAction
from PySide6.QtWidgets import QMenuBar, QFileDialog


class MenuBar(QMenuBar):
    def __init__(self):
        super().__init__()
        self.openFolderAction = QAction("Ouvrir", self)
        self.openFolderAction.setStatusTip("Ouvrir un nouveau dossier")

        # Ajout de la barre de menu
        file_menu = self.addMenu("Fichier")
        file_menu.addAction(self.openFolderAction)

    def connect_open_folder_action(self, handler):
        self.openFolderAction.triggered.connect(handler)
