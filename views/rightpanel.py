from PySide6.QtWidgets import QWidget, QFormLayout, QLineEdit, QVBoxLayout

from businesslogic.photo_infos import PhotoInfos


class ExifForm(QWidget):
    def __init__(self):
        super().__init__()
        self.setObjectName("ExifForm")
        self.setAutoFillBackground(True)

        self.layout = QFormLayout()
        self.setLayout(self.layout)

        self.dateTimeField = QLineEdit()
        self.dateTimeField.setAutoFillBackground(True)
        self.filenameField = QLineEdit()
        self.fileLocationField = QLineEdit()
        self.fileLocationField.setReadOnly(True)

        self.layout.addRow("Emplacement du fichier", self.fileLocationField)
        self.layout.addRow("Nom du fichier", self.filenameField)
        self.layout.addRow("Date de prise de vue", self.dateTimeField)

    def set_photo_infos(self, photo_infos: PhotoInfos):
        self.dateTimeField.setText(photo_infos.dateTimeOriginal)
        self.filenameField.setText(photo_infos.filename)
        self.fileLocationField.setText(photo_infos.fileLocation)


class RightPanel(QWidget):
    def __init__(self):
        super().__init__()
        self.setAutoFillBackground(True)  # Ajoutez cette ligne
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.photoInfos = None
        self.form = ExifForm()
        self.layout.addWidget(self.form)

    def set_photo_infos(self, photo_infos: PhotoInfos):
        self.photoInfos = photo_infos
        self.form.set_photo_infos(photo_infos)

    def observeChange(self,
                      *args, **kwargs):
        print(f"change observed. args{args} kwargs{kwargs}")
        if args[0] == 'image_loaded':
            image_info = kwargs.get('imageInfo', None)
            if image_info is not None:
                self.set_photo_infos(image_info)
