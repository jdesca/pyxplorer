from PIL.ImageQt import ImageQt
from PySide6.QtCore import Qt
from PySide6.QtGui import QTransform, QImage, QPixmap
from PySide6.QtWidgets import QWidget, QPushButton, QHBoxLayout, QLabel, QVBoxLayout


class BottomBar(QWidget):
    def __init__(self):
        super().__init__()

        self.button = QPushButton("R")
        self.previsouButton = QPushButton("Prev.")
        self.nextbutton = QPushButton("Next.")

        layout = QHBoxLayout()
        # layout.setContentsMargins(50,50, 0, 0)
        layout.addWidget(self.previsouButton)
        layout.addWidget(self.button)
        layout.addWidget(self.nextbutton)
        self.setLayout(layout)
        self.setMaximumHeight(50)

    def connect_button_click(self, handler):
        if callable(handler):
            print("CONNECTED")
            self.button.clicked.connect(handler)
        else:
            print("ERROR: Handler is not callable")

    def connect_next_click(self, handler):
        self.nextbutton.clicked.connect(handler)

    def connect_prev_click(self, handler):
        self.previsouButton.clicked.connect(handler)


class LeftPanel(QWidget):
    def __init__(self):
        super().__init__()

        self.pixmap = None
        self.setAutoFillBackground(True)

        self.imageLabel = QLabel("Chargment en cours")
        self.imageLabel.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.bottomBar = BottomBar()

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.imageLabel)
        layout.addWidget(self.bottomBar)
        self.setLayout(layout)

    def load_image(self, pixmap):
        self.pixmap = pixmap
        self.imageLabel.setPixmap(pixmap)
        self.update_image()

    def observeChange(self,
                      *args, **kwargs):
        print(f"change observed. args{args} kwargs{kwargs}")
        if args[0] == 'image_loaded':
            image = kwargs.get('image', None)
            if image is not None:
                image_qt = ImageQt(image)
                self.pixmap = QPixmap.fromImage(image_qt)
                self.update_image()

    def rotate_image(self, angle=90):
        transform = QTransform().rotate(angle)
        self.pixmap = self.pixmap.transformed(transform)
        self.update_image()

    def connect_button_clic(self, handler):
        self.bottomBar.connect_button_click(handler)

    def update_image(self):
        # Redimensionner l'image tout en conservant le ratio
        if self.pixmap is not None:
            scaled_pixmap = self.pixmap.scaled(self.imageLabel.size(),
                                               Qt.AspectRatioMode.KeepAspectRatio,
                                               Qt.TransformationMode.SmoothTransformation)
            self.imageLabel.setPixmap(scaled_pixmap)

    def resizeEvent(self, event):
        self.update_image()

    def connect_next_image_click(self, on_next_click):
        self.bottomBar.connect_next_click(on_next_click)

    def connect_prev_image_click(self, handler):
        self.bottomBar.connect_prev_click(handler)
