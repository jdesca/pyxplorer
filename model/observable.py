class Observable:
    def __init__(self):
        self._observers = []

    def addObserver(self, observer):
        self._observers.append(observer)

    def removeObserver(self, observer):
        self._observers.remove(observer)

    def notifyObservers(self, *args, **kwargs):
        for observer in self._observers:
            observer.observeChange(*args, **kwargs)
