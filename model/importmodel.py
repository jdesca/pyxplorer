import os

from PIL import Image

from model.observable import Observable
from businesslogic.photo_infos import PhotoInfos


class ImportModel(Observable):
    """Classe contenant l'état de l'application d'import

    Attributes :
        _directoryPath (str) : Path du dossier parcouru par l'import
        _currentImage (PIL. Image. Image) : Image en cours d'affichage
        _currentImageInfo (PhotoInfos) : Informations de l'image en cours d'affichage
        _currentImageIndex (int) : Index de l'image en cours d'affichage
        _directoryContent (list) : Liste des fichiers contenus dans le dossier
    """

    def __init__(self):
        super().__init__()
        self._directoryPath = ""
        self._currentImage = None
        self._currentImageInfo = PhotoInfos()
        self._currentImageIndex = 0
        self._directoryContent = []

    def set_directory_path(self, directory_path: str):
        self._directoryPath = directory_path
        self._currentImageIndex = 0
        self._load_directory()
        self._load_image()
        self.notifyObservers('directory_changed', directory=self._directoryPath)

    def next_page(self):
        directory_length = len(self._directoryContent)
        if directory_length > self._currentImageIndex:
            self._currentImageIndex += 1
            self._load_image()

    def previous_page(self):
        if  self._currentImageIndex > 0:
            self._currentImageIndex -= 1
            self._load_image()

    def _load_directory(self):
        valid_extensions = ['.jpg', '.jpeg']
        files = ["/".join([self._directoryPath, f]) for f in
                 os.listdir(self._directoryPath) if
                 os.path.isfile("/".join([self._directoryPath, f])) and os.path.splitext(f)[
                     1].lower() in valid_extensions]
        self._directoryContent = files

    def _load_image(self):
        if self._currentImageIndex < len(self._directoryContent):
            self._currentImage = Image.open(self._directoryContent[self._currentImageIndex])
            self._load_photo_infos()
            self.notifyObservers('image_loaded', image=self._currentImage, imageInfo=self._currentImageInfo)

    def _load_photo_infos(self):
        photo_infos = PhotoInfos()
        fullpath = self._directoryContent[self._currentImageIndex]
        photo_infos.fileLocation = '/'.join(fullpath.split('/')[:-1])
        photo_infos.filename = fullpath.split('/')[-1]
        original_date = self._currentImage.getexif().get(306)
        photo_infos.dateTimeOriginal = original_date
        self._currentImageInfo = photo_infos

