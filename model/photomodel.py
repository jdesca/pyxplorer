import os

from PIL import Image

from businesslogic.photo_infos import PhotoInfos


class PhotoModel:
    def __init__(self, directory_path):
        self.imagePath = None
        self.directory_path = directory_path
        self.image = None
        self.photo_infos = None
        self.directory_content = []
        self.displayed_page = 0
        self._load_directory()
        self.imagePath = self.directory_content[self.displayed_page]
        self.load_image()


    def load_image(self):
        self.image = Image.open(self.imagePath)
        self.photo_infos = self._load_photo_infos()

    def _load_photo_infos(self) -> PhotoInfos:
        photo_infos = PhotoInfos()
        photo_infos.fileLocation = '/'.join(self.imagePath.split('/')[:-1])
        photo_infos.filename = self.imagePath.split('/')[-1]
        exif_info = self.image.getexif().get(306)
        photo_infos.dateTimeOriginal = exif_info
        return photo_infos

    def _load_directory(self):
        files = ["/".join([self.directory_path, f]) for f in
                 os.listdir(self.directory_path) if
                 os.path.isfile("/".join([self.directory_path, f]))]
        self.directory_content = files

    def get_image(self) -> Image:
        return self.image

    def get_photo_infos(self) -> PhotoInfos:
        return self.photo_infos

    def set_displayed_page(self, param):
        self.displayed_page = param
        self.imagePath = self.directory_content[param]
        self.load_image()

    def next_page(self):
        directory_count = len(self.directory_content)
        print(directory_count)
        print(self.displayed_page)
        if directory_count > self.displayed_page:
            print("+1")
            self.displayed_page = self.displayed_page + 1
            print(self.displayed_page)

        self.imagePath = self.directory_content[self.displayed_page]

    def previous_page(self):
        directory_count = len(self.directory_content)
        if self.displayed_page > 0 :
            self.displayed_page = self.displayed_page - 1

        self.imagePath = self.directory_content[self.displayed_page]

