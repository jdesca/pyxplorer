from PySide6.QtWidgets import QFileDialog

from model.importmodel import ImportModel
from views.leftpanel import LeftPanel
from views.menubar import MenuBar
from views.views import RightPanel


class LeftPanelController:
    """
    Contrôleur du panneau gauche de l'application (affichage des photo et navigation)
    """

    def __init__(self, model: ImportModel, left_panel: LeftPanel):
        self.model = model
        self.leftPanel = left_panel
        self.leftPanel.connect_button_clic(self.on_rotate_click)
        self.leftPanel.connect_next_image_click(self.on_next_image_click)
        self.leftPanel.connect_prev_image_click(self.on_prev_image_click)

    def on_rotate_click(self):
        self.leftPanel.rotate_image()

    def on_next_image_click(self):
        self.model.next_page()

    def on_prev_image_click(self):
        self.model.previous_page()


class RightPanelController:
    def __init__(self, model: ImportModel, right_panel: RightPanel):
        self.rightPanel = right_panel
        self.model = model


class MenuBarController:
    def __init__(self, model: ImportModel, menu_bar: MenuBar):
        self.model = model
        self.menuBar = menu_bar
        self.menuBar.connect_open_folder_action(self.open_folder_action)

    def open_folder_action(self):
        folder = QFileDialog.getExistingDirectory(self.menuBar, "Veuillez selectionné un dossier")
        if folder:
            print(folder)